package eCommerceAdmin.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eCommerceAdmin.classi.Store;
import eCommerceAdmin.classi.Utente;
import eCommerceAdmin.dao.StoreDao;
import eCommerceAdmin.dao.UtenteDao;

/**
 * Servlet implementation class UpdateUser
 */
@WebServlet("/updateuser")
public class UpdateUser extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String username = request.getParameter("user");
		String indirizzo = request.getParameter("indirizzo");
		String psw = request.getParameter("psw");
		
		Utente newSto = new Utente();
		
		newSto.setIndirizzo(indirizzo);
		newSto.setUsername(username);
		
		newSto.setPsw(psw);
		
		UtenteDao stoDao = new UtenteDao();
		
		try {
			if(stoDao.update(newSto))
			{
				PrintWriter out = response.getWriter();
				out.print(true);
			}
			else
			{
				PrintWriter out = response.getWriter();
				out.print(false);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
