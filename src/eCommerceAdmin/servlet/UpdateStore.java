package eCommerceAdmin.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eCommerceAdmin.classi.Store;
import eCommerceAdmin.dao.StoreDao;

/**
 * Servlet implementation class UpdateStore
 */
@WebServlet("/updatestore")
public class UpdateStore extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nome = request.getParameter("nome");
		String indirizzo = request.getParameter("indirizzo");
		String url_immagine = request.getParameter("url_immagine");
		String codice  = request.getParameter("codice");
		Store newSto = new Store();
		
		newSto.setIndirizzo(indirizzo);
		newSto.setNome(nome);
		newSto.setUrl_immagine(url_immagine);
		newSto.setCodice(codice);
		
		StoreDao stoDao = new StoreDao();
		
		try {
			if(stoDao.update(newSto))
			{
				PrintWriter out = response.getWriter();
				out.print(true);
			}
			else
			{
				PrintWriter out = response.getWriter();
				out.print(false);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
