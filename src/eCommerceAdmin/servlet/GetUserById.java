package eCommerceAdmin.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import eCommerceAdmin.classi.Utente;

import eCommerceAdmin.dao.UtenteDao;

/**
 * Servlet implementation class GetUserById
 */
@WebServlet("/getuserbyid")
public class GetUserById extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UtenteDao uteDao = new UtenteDao();
		String codice = request.getParameter("codice");
		try {
			Utente ute = uteDao.getByCod(codice);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			Gson Json = new Gson();
			String uteJson = Json.toJson(ute);
			out.print(uteJson);
		} catch (SQLException e) {
			System.out.println("get All store: " + e.getMessage());
			e.printStackTrace();
		}

	}
}
