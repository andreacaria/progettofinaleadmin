package eCommerceAdmin.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import eCommerceAdmin.classi.Amministratore;
import eCommerceAdmin.dao.AmministratoreDao;

/**
 * Servlet implementation class VerificaAdmin
 */
@WebServlet("/verificaadmin")
public class VerificaAdmin extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AmministratoreDao amDao = new AmministratoreDao();
		try {
			String user = request.getParameter("user");
			String pass = request.getParameter("pass");
		
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			Amministratore admin = new Amministratore();
			Gson Json = new Gson();
			admin.setPsw(pass);
			admin.setUsername(user);
			amDao.checkAdmin(admin);
			if(admin != null) {
				HttpSession sessione = request.getSession();
				sessione.setAttribute("role", "admin");
				sessione.setAttribute("user", user);
				String AdminJson = Json.toJson(admin);
				out.print(AdminJson);
				
			}
			
		}catch (NullPointerException e) {
			System.out.println(e.getMessage());
			e.getStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("DB: "+ e.getMessage());
			e.getStackTrace();
		}

		
		
	}

}
