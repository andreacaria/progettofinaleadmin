package eCommerceAdmin.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import eCommerceAdmin.classi.Store;
import eCommerceAdmin.dao.AmministratoreDao;
import eCommerceAdmin.dao.StoreDao;

/**
 * Servlet implementation class GetStoreByCod
 */
@WebServlet("/getstorebycod")
public class GetStoreByCod extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StoreDao storDao = new StoreDao();
		String codice = request.getParameter("codice");
		try {
			Store negozio = storDao.getByCod(codice);
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			Gson Json = new Gson();
			String negozioJson = Json.toJson(negozio);
			out.print(negozioJson);
		} catch (SQLException e) {
			System.out.println("get All store: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
