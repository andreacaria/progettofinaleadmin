package eCommerceAdmin.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eCommerceAdmin.classi.Store;
import eCommerceAdmin.dao.StoreDao;

/**
 * Servlet implementation class DeleteByCod
 */
@WebServlet("/deletebycod")
public class DeleteByCod extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		StoreDao storDao = new StoreDao();
		String codice = request.getParameter("codice");
		try {
			Store negozio = storDao.getByCod(codice);
			if(storDao.delete(negozio)) {
				PrintWriter out = response.getWriter();
				out.print(true);
			}
		} catch (SQLException e) {
			System.out.println("Delete by cod: " +e.getMessage());
			e.printStackTrace();
		}
		
		
	}

}
