package eCommerceAdmin.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import eCommerceAdmin.classi.Store;
import eCommerceAdmin.dao.AmministratoreDao;
import eCommerceAdmin.dao.StoreDao;



/**
 * Servlet implementation class GetAllStore
 */
@WebServlet("/getallstore")
public class GetAllStore extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			StoreDao storDao = new StoreDao();
			AmministratoreDao io = new AmministratoreDao();
		
			try {
				ArrayList<Store> elencoStore = storDao.getAll();
				PrintWriter out = response.getWriter();
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				Gson Json = new Gson();
				String ElencoJson = Json.toJson(elencoStore);
				out.print(ElencoJson);
			} catch (SQLException e) {
				System.out.println("get All store: " + e.getMessage());
				e.printStackTrace();
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
