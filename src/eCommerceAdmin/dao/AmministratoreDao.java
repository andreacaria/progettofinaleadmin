package eCommerceAdmin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import eCommerceAdmin.classi.Amministratore;
import eCommerceAdmin.classi.Utente;
import eCommerceAdmin.connessione.Connettore;

public class AmministratoreDao implements Dao<Amministratore> {

	@Override
	public Amministratore getById(int ID) throws SQLException {
		return null;
	}

	@Override
	public Amministratore getByCod(String codice) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Amministratore> getAll() throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean insert(Amministratore t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Amministratore t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Amministratore t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}
	public Amministratore checkAdmin(Amministratore t) throws SQLException{
		Connection conn = Connettore.getIstanza().getConnessione();
		String query= "	select amministratoreid, username, psw  from amministratore where username = ? and psw = ?;";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1,t.getUsername());
		ps.setString(2,t.getPsw());
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		
		t.setAmministratoreid(risultato.getInt(1)); 
		t.setUsername(risultato.getString(2)); 
		t.setPsw(risultato.getString(3)); 
		return t;
	    
		
		
	}

}
