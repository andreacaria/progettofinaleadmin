package eCommerceAdmin.classi;

public class Utente {
	private int utenteid  ;
	private String username  ;
	private String psw ;
	private String indirizzo ;
	
	public Utente() {
		
	}
	public int getUtenteid() {
		return utenteid;
	}
	public void setUtenteid(int utenteid) {
		this.utenteid = utenteid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPsw() {
		return psw;
	}
	public void setPsw(String psw) {
		this.psw = psw;
	}
	public String getIndirizzo() {
		return indirizzo;
	}
	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}
	public Utente(int utenteid, String username, String psw, String indirizzo) {
		super();
		this.utenteid = utenteid;
		this.username = username;
		this.psw = psw;
		this.indirizzo = indirizzo;
	}
	
	
}
