package eCommerceAdmin.classi;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Ordine {
	private int ordineid    ;
	private String codiceOrdine    ;
	private int storerif   ;
	private int utenterif   ;
	private Utente user;
	private LocalDateTime orario;
	private ArrayList<Prodotto> elencoProdotti;
	
	public ArrayList<Prodotto> getElencoProdotti() {
		return elencoProdotti;
	}
	public void setElencoProdotti(ArrayList<Prodotto> elencoProdotti) {
		this.elencoProdotti = elencoProdotti;
	}

	public int getOrdineid() {
		return ordineid;
	}
	public void setOrdineid(int ordineid) {
		this.ordineid = ordineid;
	}
	public String getCodiceOrdine() {
		return codiceOrdine;
	}
	public void setCodiceOrdine(String codiceOrdine) {
		this.codiceOrdine = codiceOrdine;
	}
	public int getStorerif() {
		return storerif;
	}
	public void setStorerif(int storerif) {
		this.storerif = storerif;
	}
	public int getUtenterif() {
		return utenterif;
	}
	public void setUtenterif(int utenterif) {
		this.utenterif = utenterif;
	}
	public LocalDateTime getOrario() {
		return orario;
	}
	public void setOrario(LocalDateTime orario) {
		this.orario = orario;
	}
	public Utente getUser() {
		return user;
	}
	public void setUser(Utente user) {
		this.user = user;
	}
	public ArrayList<Prodotto> getListaProdotto() {
		return listaProdotto;
	}
	public void setListaProdotto(ArrayList<Prodotto> listaProdotto) {
		this.listaProdotto = listaProdotto;
	}
	private ArrayList<Prodotto> listaProdotto;

}
