package eCommerceAdmin.classi;

public class Amministratore {
	private int amministratoreid ;
	private String username;
	private String psw;
	
	public Amministratore() {
		
	}
	
	public Amministratore(int amministratoreid, String username, String psw) {
		super();
		this.amministratoreid = amministratoreid;
		this.username = username;
		this.psw = psw;
	}
	public int getAmministratoreid() {
		return amministratoreid;
	}
	public void setAmministratoreid(int amministratoreid) {
		this.amministratoreid = amministratoreid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPsw() {
		return psw;
	}
	public void setPsw(String psw) {
		this.psw = psw;
	}
	
}
