/**
 * 
 */
$(document).ready(
	function(){
		Negozi();
		Utenti();
	}
	
	
)
function Utenti(){
	$.ajax(
		
			{
					url: "http://localhost:8080/eCommerceAdmin/getalluser",
					method: "GET",
					success: function(risultato) {	 
						CardUte(risultato);
					},
					error: function(errore){
						alert("errore");	
					}
				}
	)
	
	
	
}
function Negozi(){
	$.ajax(
		
			{
					url: "http://localhost:8080/eCommerceAdmin/getallstore",
					method: "GET",
					
					
					success: function(risultato) {
						
						createCardNegozzi(risultato);

					},
					error: function(errore){
						alert("errore");
						
					}
				}
	)
	
	
	
}
function CardUte(objUtente){
	let cont = "";
	for(let i =0; i<objUtente.length; i++){
		cont += cardSingoloUtente(objUtente[i]);
		}
		$("#cardUtente").html(cont);
	
}
function cardSingoloUtente(utente){
	let cont= "";
	cont += '<div class="card m-1 d-flex align-items-center"" style="width: 150px; height: 150px; border-radius: 25% ;">';
	cont += '<div class="card-body">';
	cont += '<h5 class="card-title">'+ utente.username+'</h5>';
	cont += '<div class = "d-flex justify-content-around">'
	cont += '<button data-codice =\''+utente.username+'\' type= "button" class="btn btn-outline-danger bottoniTondi" onclick = "eliminaUSER(this)"> <i class="fas fa-trash-alt"></i> </button>'
  	cont += '<button data-codice =\''+utente.username+'\' type = "button" class="btn btn-outline-warning bottoniTondi" onclick = "modificaUSER(this)"> <i class="fas fa-pencil-ruler"></i></button>';
	cont += '<button data-codice = \''+utente.username+'\'type = "button" class="btn btn-outline-info bottoniTondi" onclick = "infoUSER(this)"> <i class="fas fa-info"></i> </button>';
	cont += '</div>';
    cont += '</div></div>'
return cont;
}





function createCardNegozzi(objNegozzi){
	let cont = "";
	for(let i =0; i<objNegozzi.length; i++){
		cont += cardSingolo(objNegozzi[i]);
		}
		
		$("#cardNegozi").html(cont);
	
}
function cardSingolo(objNegozio){
	let cont= "";
	cont += '<div class="card m-1" style="width: 200px; border-radius: 10% ; border-color: white; ">';
	cont += ' <img style="border-top-left-radius: 10%; border-top-right-radius: 10%;  height: 60% ;"  class="card-img-top" src="'+objNegozio.url_immagine+'" alt="Card image cap" style = "width: 100%; height: 70%">';
	cont += '<div class="card-body">';
	cont += ' <h5 class="card-title">'+ objNegozio.nome+'</h5>';
	cont += '<div class = "d-flex justify-content-around">'
	cont += '<button data-codice =\''+objNegozio.codice+'\' type= "button" class="btn btn-outline-danger bottoniTondi" onclick = "elimina(this)"> <i class="fas fa-trash-alt"></i> </button>'
  	cont += '<button data-codice =\''+objNegozio.codice+'\' type = "button" class="btn btn-outline-warning bottoniTondi" onclick = "modifica(this)"> <i class="fas fa-pencil-ruler"></i></button>';
	cont += '<button data-codice = "'+objNegozio.codice+'" type = "button" class="btn btn-outline-info bottoniTondi" onclick = "info(this)"> <i class="fas fa-info"></i> </button>';
	cont += '</div>';
    cont += '</div></div>'
return cont;

	
	
}
function getStoreByCod(codice,tipo){
	$.ajax(
			{
					url: "http://localhost:8080/eCommerceAdmin/getstorebycod",
					method: "POST",
					data:{
						codice : codice
					},
					
					success: function(risultato) {
						attivaModale(risultato,tipo);
						

					},
					error: function(errore){
						alert("errore");
						
					}
				}
		)
}
function getUserByUser(codice,tipo){
	$.ajax(
			{
					url: "http://localhost:8080/eCommerceAdmin/getuserbyid",
					method: "POST",
					data:{
						codice : codice
					},
					
					success: function(risultato) {
						attivaModaleUtente(risultato,tipo);
						

					},
					error: function(errore){
						alert("errore");
						
					}
				}
		)
	
}

function attivaModaleUtente(utenteObj,tipo){
	
	switch (tipo){
		case "elimina":
			$("#modal").modal("show");
			$("#modalTitle").html("<strong>Eliminare</strong> "+utenteObj.username+" ?");
			let contButelimina = '<button type="button" class="btn btn-danger bottoniTondi" data-dismiss="modal"><i class="fas fa-times"></i></button>';
			contButelimina += '<button onclick="eliUser(\''+utenteObj.username+'\')" type="button" class="btn btn-success bottoniTondi" ><i class="fas fa-check"></i></button>';
			$("#modalButton").html(contButelimina);	
		
		
			break;
		
		case "info":
			$("#modal").modal("show");
			$("#modalTitle").html(utenteObj.nome);
		
			let info = '<div class="card" style="width: 100%; height: 300px">';
			info += '<div class="card-body">';
			
			info += '<p>Indirizzo: '+utenteObj.indirizzo+'</p>';
	    	info += '</div></div>' 
			$("#modalBody").html(info);
			$("#modalButton").html("");	
			break;

		
		
		case "modifica":
		
			$("#modal").modal("show");
			$("#modalTitle").html(utenteObj.username);
			let modifica = '<div class="card" style="width: 100%; height: 300px">';
			modifica += '<div class="card-body">';
			modifica += '<div class="form-group">';
			modifica+= ' <label for="inputPassword">Password</label>';
			modifica += '<input type="text" value = "'+utenteObj.psw+'" class="form-control" id="inputPassword" placeholder="Nuovo Nome">';
			modifica+= ' <label for="inputIndirizzo">Indirizzo</label>';
			modifica += '<input type="text" value = "'+utenteObj.indirizzo+'" class="form-control" id="inputIndirizzo" placeholder="Nuovo Indirizzo">';
			modifica += '</div>';
		    modifica += '</div></div>' 
			$("#modalBody").html(modifica);
			let contButmodifica = '<button type="button" class="btn btn-danger bottoniTondi" data-dismiss="modal"><i class="fas fa-times"></i></button>';
			contButmodifica += '<button onclick="modUser(\''+utenteObj.username+'\')" type="button" class="btn btn-success bottoniTondi" ><i class="fas fa-check"></i></button>';
			$("#modalButton").html(contButmodifica);	
		
		break;
	}
	}

function attivaModale(negozioObj,tipo){
	
	switch (tipo){
		case "elimina":
			$("#modal").modal("show");
			$("#modalTitle").html("<strong>Eliminare</strong> "+negozioObj.nome+" ?");
			let elimina = '<div class="card" style="width: 100%; height: 300px">';
			elimina += '  <img class="card-img-top" src="'+negozioObj.url_immagine+'" alt="Card image cap" style = "width: 100%; height: 50%">';
			elimina += '<div class="card-body">';
			elimina += '<h5 class="card-title">'+ negozioObj.codice+'</h5>';
			elimina += '<p>Indirizzo: '+negozioObj.indirizzo+'</p>';
    		elimina += '</div></div>' 
			$("#modalBody").html(elimina);
			let contButelimina = '<button type="button" class="btn btn-danger bottoniTondi" data-dismiss="modal"><i class="fas fa-times"></i></button>';
			contButelimina += '<button onclick="eli(\''+negozioObj.codice+'\')" type="button" class="btn btn-success bottoniTondi" ><i class="fas fa-check"></i></button>';
			$("#modalButton").html(contButelimina);	
		
		
			break;
		
		case "info":
			$("#modal").modal("show");
			$("#modalTitle").html(negozioObj.nome);
		
			let info = '<div class="card" style="width: 100%; height: 300px">';
			info += '  <img class="card-img-top" src="'+negozioObj.url_immagine+'" alt="Card image cap" style = "width: 100%; height: 50%">';
			info += '<div class="card-body">';
			info += '<h5 class="card-title">'+ negozioObj.codice+'</h5>';
			info += '<p>Indirizzo: '+negozioObj.indirizzo+'</p>';
	    	info += '</div></div>' 
			$("#modalBody").html(info);
			$("#modalButton").html("");	
			break;

		
		
		case "modifica":
		
			$("#modal").modal("show");
			$("#modalTitle").html(negozioObj.nome);
			let modifica = '<div class="card" style="width: 100%; height: 300px">';
			modifica += '<div class="card-body">';
			modifica += '<div class="form-group">';
			modifica+= ' <label for="inputNome">Nome</label>';
			modifica += '<input type="text" value = "'+negozioObj.nome+'" class="form-control" id="inputNome" placeholder="Nuovo Nome">';
			modifica+= ' <label for="inputUrl">Url immagiine</label>';
			modifica += '<input type="text" value = "'+negozioObj.url_immagine+'" class="form-control" id="inputUrl" placeholder="Nuovo URL">';
			modifica+= ' <label for="inputIndirizzo">Indirizzo</label>';
			modifica += '<input type="text" value = "'+negozioObj.indirizzo+'" class="form-control" id="inputIndirizzo" placeholder="Nuovo Indirizzo">';
			modifica += '</div>';
		    modifica += '</div></div>' 
			$("#modalBody").html(modifica);
			let contButmodifica = '<button type="button" class="btn btn-danger bottoniTondi" data-dismiss="modal"><i class="fas fa-times"></i></button>';
			contButmodifica += '<button onclick="mod(\''+negozioObj.codice+'\')" type="button" class="btn btn-success bottoniTondi" ><i class="fas fa-check"></i></button>';
			$("#modalButton").html(contButmodifica);	
		
		break;
	}
		
	
}
function eliUser(codice){
	$.ajax(
			{
				url: "http://localhost:8080/eCommerceAdmin/deleteuserbyuser",
				method: "POST",
				data:{
					codice:codice
				},
				
				success: function(risultato) {
					console.log(risultato);
					if(risultato){
						Swal.fire("Eliminazione Utente effetuata con successo!")
						$("#modal").modal("hide");
						Utenti();
					}
				},
				error: function(errore){
					alert("errore");
				}
			}
		)
}
function eli(codice){
	$.ajax(
			{
				url: "http://localhost:8080/eCommerceAdmin/deletebycod",
				method: "POST",
				data:{
					codice:codice
				},
				
				success: function(risultato) {
					console.log(risultato);
					if(risultato){
						Swal.fire("Eliminazione effetuata con successo!")
						$("#modal").modal("hide");
						Negozi();
					}
				},
				error: function(errore){
					alert("errore");
				}
			}
		)
}
function modUser(user){
	let psw = $("#inputPassword").val();
	let indirizzo = $("#inputIndirizzo").val();
	
	$.ajax(
		{
			url: "http://localhost:8080/eCommerceAdmin/updateuser",
					method: "POST",
					data:{
						user:user,
						indirizzo:indirizzo,
						psw:psw
						
					},
					
					success: function(risultato) {
						console.log(risultato);
						if(risultato){
							Swal.fire("Modifica effetuata con successo!")
							$("#modal").modal("hide");
							Negozi();
						}
					},
					error: function(errore){
						alert("errore");
					}
		}
	)
}
function mod(codice){
	let nome = $("#inputNome").val();
	let indirizzo = $("#inputIndirizzo").val();
	let url_immagine = $("#inputUrl").val();
	$.ajax(
		{
			url: "http://localhost:8080/eCommerceAdmin/updatestore",
					method: "POST",
					data:{
						codice:codice,
						indirizzo:indirizzo,
						nome:nome,
						url_immagine:url_immagine
					},
					
					success: function(risultato) {
						console.log(risultato);
						if(risultato){
							Swal.fire("Modifica effetuata con successo!")
							$("#modal").modal("hide");
							Negozi();
						}
					},
					error: function(errore){
						alert("errore");
					}
		}
	)
}
function info(objButton){
	
	let codice = $(objButton).data("codice");
	getStoreByCod(codice,"info");
}
function modifica(objButton){
	
	let codice = $(objButton).data("codice");
	getStoreByCod(codice,"modifica");
}

function elimina(objButton){
	
	let codice = $(objButton).data("codice");
	getStoreByCod(codice,"elimina");
}

function infoUSER(objButton){
	
	let codice = $(objButton).data("codice");
	getUserByUser(codice,"info");
}
function modificaUSER(objButton){
	
	let codice = $(objButton).data("codice");
	getUserByUser(codice,"modifica");
}

function eliminaUSER(objButton){
	
	let codice = $(objButton).data("codice");
	getUserByUser(codice,"elimina");
}