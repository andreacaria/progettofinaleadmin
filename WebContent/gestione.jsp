<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%
    HttpSession sessione = request.getSession();
	//String ruolo_utente = (String)sessione.getAttribute("role") != null ? (String)sessione.getAttribute("role") : "";
	
	//Metodo equivalente all'operatore ternario
	String ruolo = "";
	String user = "";
	if((String)sessione.getAttribute("role") != null){
		ruolo = (String)sessione.getAttribute("role");
	}
	if((String)sessione.getAttribute("user") != null){
		user = (String)sessione.getAttribute("user");
	}
	
	if(!ruolo.equals("admin"))
		response.sendRedirect("admin.jsp");
    
    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="CSS/admin.css"> 
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"> 
</head>
</head>
<body>
<div class = "container m-5">
	<div class= "row">
		<div class = "col">
		<h3>Negozi: </h3>
		<div class= "alert alert-info"> Attraverso questo sezione � possibile effettuare delle modifiche sugli store presenti nel sito, � quindi possibile eliminare o modificare o introdurre uno store </div>
		<div class="d-flex align-content-start flex-wrap" id = "cardNegozi">
		</div>
		</div>
	</div>
</div>
<br>
<div class = "container m-5">
	<div class= "row">
		<div class = "col">
		<h3>Utenti: </h3>
		<div class= "alert alert-warning"> Attraverso questo sezione � possibile effettuare delle modifiche sugli <strong>Utenti</strong>trong presenti nel sito, � quindi possibile eliminare o modificare o introdurre uno store </div>
		<div class="d-flex align-content-start flex-wrap" id = "cardUtente">
		</div>
		
		</div>
	</div>
</div>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id= "modalTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id= "modalBody">
       
      </div>
      <div class="modal-footer" id = "modalButton">
        
      </div>
    </div>
  </div>
</div>

	<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<script src="JS/gestione.js"></script>
		<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</body>
</html>