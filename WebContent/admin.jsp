<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Login</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="CSS/admin.css"> 
</head>
<body>
	<div class= "container m-5">
		
		<div class = "col">
			<div class="d-flex justify-content-center align-items-center">
				<div class="card cardLogin bg-warning d-flex justify-content-center align-items-center" >

					<div class="card-body d-flex justify-content-center align-items-center ">
						<div class = "p-2">
					    	<div class="form-group">
						    	
			    					<label for="adminUser">Username: </label>
			    					<input type="text" class="form-control" id="adminUser" placeholder="Username">
			    					<label for="adminPass">Password: </label>
			    					<input type="password" class="form-control" id="adminPass" placeholder="Password">
			  				
		  					</div>
		  					<button type = "button"  onclick= "checkAdmin()" class = "btn btn-outline-info d-flex justify-content-center" >LogIn</button>
	  					</div>
				  	</div>
				</div>	
			</div>					
		</div>
		<div id = "alertCampi" style = "display: none; min-width:300px" class = "alert alert-danger m-5" >
		Nome utente o Password vuoti!!
		</div>
		
		</div>
		
	
	</div>
		<script src="https://code.jquery.com/jquery-3.6.0.js"
		integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
		integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
		crossorigin="anonymous"></script>
	<script src="JS/admin.js"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</body>
</html>